import React from 'react';
import './App.css';

import { Login } from "./components/login";
import { Menu } from "./components/Menu.jsx"

export class App extends React.Component {

	constructor(props) {
		super(props);

		this.state = this.getInitialSate();
		this.logIn = this.logIn.bind(this);
	}

	getInitialSate() {
		return {
			isLoggedIn: localStorage.getItem('isLoggedIn')
		}
	}

	logIn() {
		this.setState({
			isLoggedIn: true
		});
	}


	render() {
		let view = (this.state.isLoggedIn === true) ? <Menu /> : <Login logIn={this.logIn}/>;
		return (view);
	}
}

export default App;
