import React, { Component } from 'react';
import { MDBIcon } from 'mdbreact'
import logo from '../resource/payULogo.png'

import 'bootstrap/dist/css/bootstrap.css';
import '../css/Header.css';
import '../css/Menu.css';

import { Form } from "./Form.jsx";
import { SideMenu } from "./SideMenu.jsx";


export class Menu extends Component {
    constructor(props) {
        super(props);

        this.state = this.getInitialSate();
    }

    getInitialSate() {
        return {
            page: 0
        }
    }

    render() {
        return (
            <div id="menu">
                <div className="color-line ng-scope" />

                <nav class="navbar navbar-expand-lg navbar-light" style={{zIndex: "50"}}>
                    <a class="navbar-brand" href="#">
                        <img src={logo} width="auto" height="60" class="d-inline-block align-top" alt="" />
                    </a>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <form class="form-inline my-2 my-lg-0">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item navButton">
                                    <MDBIcon icon="cog" />
                                    <a class="nav-link" href="#">Configuración</a>
                                </li>

                                <li class="nav-item navButton">
                                    <MDBIcon icon="search" />
                                    <a class="nav-link" href="#">Ayuda y soporte</a>
                                </li>

                                <li class="nav-item dropdown navDrop">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span style={{ marginRight: "3px" }}><MDBIcon icon="user-alt" /></span>
                                        secure@pagosonline.com
                                </a>
                                    <small>Administrador - 1</small>

                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </nav>

                <div className="row" style={{ width: "100%" }}>
                    <div className="col-2">
                        <SideMenu />
                    </div>

                    <div className="col-10">
                        {(this.state.page == 0) ? <Form /> : <Form />}
                    </div>
                </div>
            </div>
        )
    }
}