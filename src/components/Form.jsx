import React, { Component } from 'react';
import * as axios from "axios";

import '../css/Form.css';
import '../css/Form.css';

const categorias = require('../json/reportTypes.json');
const schedules = require('../json/schedule.json');
const dateFormat = require('../json/dateFormat.json');

export class Form extends Component {
    constructor(props) {
        super(props);

        this.state = this.getInitialSate();
        this.handleChange = this.handleChange.bind(this);
        this.getCategories = this.getCategories.bind(this);
        this.handleTypeCheck = this.handleTypeCheck.bind(this);
        this.preview = this.preview.bind(this);
    }

    getInitialSate() {
        return {
            filterCatsOpts: [],

            types: [],
            heads: [],

            typesMap: new Map(),
            typesChk: new Map(),

            delimitador: "DOUBLE_QUOTE",
            email: "",
            finesDeSemana: false,
            formaEnvio: "x",
            horario: "DAILY_ONE",
            hora: 0,
            formato: "DD_MM_YYYY_HH24_MI_SS",
            nombreArchivo: "",
            separador: "COMMA",
            reportType: "MOVEMENTS",
            zip: false
        }
    }

    preview() {
        let correos = this.state.email.replace(/\s/g, '').split(',');

        let resp = {
            "csvFileNameFormat": (this.state.nombreArchivo + "_{accountId}_{day}-{month}-{year}.csv"),
            "deliveryEmails": correos,
            "schedule": this.state.horario,
            "executionHour": this.state.hora,
            "dateFormat": this.state.formato,
            "shouldExcludeWeekends": this.state.finesDeSemana,
            "compressionMethod": (this.state.zip) ? "zip" : null,
            "csvFieldDelimiter": this.state.delimitador,
            "csvFieldSeparator": this.state.separador,
            "reportType": this.state.reportType,
            "csvFields": Array.from(this.state.typesMap.keys()),
            "csvHeaders": Array.from(this.state.typesMap.values())
        }

        let accountId = 1;
        let reportType = this.state.reportType;
        let startDate = "2019-12-10";
        let endDate = "2019-12-12";
        let url = "https://api.payulatam.com/secure-api/merchant-reports/report-generation/request-report/" + accountId + "/" + reportType + "?startDate=" + startDate + "&endDate=" + endDate;

        console.log(url);
        console.log(resp);

        axios.post(url, JSON.stringify(resp), {
            headers: {
                Authorization: localStorage.getItem('accessToken'),
                "Content-Type": "application/json"
            },
        }).then(function (response) {
            alert("Enviado correctamente")
        }).catch(function (error) {
            alert("Error al enviar");
            console.log(error);
        });
    }

    getCategories() {
        let opts = this.state.filterCatsOpts;
        let cate = this.state.reportType;

        if (opts.length === 0) {
            opts = categorias[cate];
        }

        let table = (
            <div className="scroll">
                <table className="table table-bordered table-hover">
                    <thead className="thead-payu">
                        <tr>
                            <th scope="col">Id del Campo</th>
                            <th scope="col">Nombre en el reporte</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            opts.map((c, i) => {
                                return (
                                    <tr key={"ROW_" + cate + "-" + c}>
                                        <td>
                                            <div className="custom-control custom-checkbox">
                                                <input type="checkbox" className="custom-control-input" id={"CHK_" + cate + "-" + c} name={cate + "-" + c} value={this.state.typesChk.get(cate + "-" + c)} onChange={this.handleTypeCheck} />
                                                <label className="custom-control-label" id={"LBL_" + cate + "-" + c} htmlFor={"CHK_" + cate + "-" + c}>{c}</label>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" className="form-control" id={"HED_" + cate + "-" + c} name={cate + "-" + c} onChange={this.handleTypeCheck} />
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        );

        return table;
    }

    handleChange(e) {
        let filtered = this.state.filterCatsOpts;

        if (e.target.name === 'filter' || e.target.name === 'reportType') {
            const cat = categorias[this.state.reportType];
            const filt = e.target.value.toUpperCase();

            filtered = [];

            cat.forEach((o, i) => {
                if (o.startsWith(filt)) {
                    filtered.push(o);
                }
            });
        }

        this.setState({
            [e.target.name]: (e.target.type === 'radio') ? !this.state[e.target.name] : e.target.value,
            filterCatsOpts: filtered
        });
    }

    handleTypeCheck(e) {
        let isCheck = ""
        let type = "";
        let head = "";

        let typesChk = this.state.typesChk;
        let typesMap = this.state.typesMap;
        let types = this.state.types;
        let heads = this.state.heads;

        if (e.target.type === 'checkbox') {
            isCheck = e.target.checked;
            type = e.target.name.split('-')[1];
            typesChk.set(type, isCheck);
            head = document.getElementById("HED_" + e.target.name).value;
        } else {
            type = e.target.name.split('-')[1];;
            head = e.target.value;
            isCheck = document.getElementById("CHK_" + e.target.name).checked;
        }

        if (isCheck === true && head.replace(/\s/g, '').length > 0) {
            typesMap.set(type, head);
        } else {
            typesMap.delete(type)
        }

        this.setState({
            typesChk: typesChk,
            typesMap: typesMap,
            types: types,
            heads: heads
        })
    }

    render() {
        const keys = Object.keys(categorias);
        const horarios = schedules.schedule;
        const dateF = dateFormat.dateFormat;

        return (
            <div className="container" style={{ marginTop: "15px" }}>
                <h1 className="title">Programación de reportes</h1>
                <h2 className="subtitle">En este módulo podrás programar tus reportes y enviarlos por email o FTP.</h2>

                <hr className="my-4" />

                <form>
                    <div className="form-row fRow">
                        <label htmlFor="colFormLabelSm" className="col-2 col-form-label">Nombre Archivo</label>

                        <div className="col-7">
                            <input type="text" className="form-control" name="nombreArchivo" value={this.state.nombreArchivo} onChange={this.handleChange} />
                        </div>

                        <select className="custom-select col-3" name="formaEnvio" value={this.state.formaEnvio} onChange={this.handleChange}>
                            <option value="x">e-mail</option>
                            <option value="y">ftp</option>
                        </select>
                    </div>

                    <div className="form-row fRow">
                        <label htmlFor="colFormLabelSm" className="col-2 col-form-label">Correos (,)</label>

                        <div className="col-10">
                            <input type="email" className="form-control" name="email" value={this.state.email} onChange={this.handleChange} />
                        </div>
                    </div>

                    <div className="form-row fRow">
                        <div className="form-row col-4">
                            <label htmlFor="colFormLabelSm" className="col-4 col-form-label">Horario</label>

                            <div className="col-8">
                                <select className="custom-select" name="horario" value={this.state.horario} onChange={this.handleChange}>
                                    {horarios.map((h, i) => {
                                        return <option key={"Sch_" + (i + 1)} value={h}>{h}</option>
                                    })}
                                </select>
                            </div>
                        </div>

                        <div className="form-row col-3">
                            <label htmlFor="colFormLabelSm" className="col-4 col-form-label">Hora</label>

                            <div className="col-8">
                                <input type="number" min="0" max="23" step="1" className="form-control" name="hora" value={this.state.hora} onChange={this.handleChange} />
                            </div>
                        </div>

                        <div className="form-row col-5">
                            <label htmlFor="colFormLabelSm" className="col-4 col-form-label">Formato</label>

                            <div className="col-8">
                                <select className="custom-select" name="formato" value={this.state.formato} onChange={this.handleChange}>
                                    {dateF.map((d, i) => {
                                        return <option key={"Daf_" + (i + 1)} value={d}>{d}</option>
                                    })}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="form-row fRow">
                        <div className="form-row col-6">
                            <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" name="finesDeSemana" checked={this.state.finesDeSemana} onClick={this.handleChange} />
                                <label className="form-check-label">Excluir fines de semana</label>
                            </div>
                        </div>

                        <div className="form-row col-6">
                            <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" name="zip" checked={this.state.zip} onClick={this.handleChange} />
                                <label className="form-check-label">Comprimir Zip</label>
                            </div>
                        </div>
                    </div>

                    <div className="form-row fRow">
                        <div className="form-row col-6">
                            <label htmlFor="colFormLabelSm" className="col-3 col-form-label">Delimitador</label>

                            <div className="col-9">
                                <select className="custom-select" name="delimitador" value={this.state.delimitador} onChange={this.handleChange}>
                                    <option value="DOUBLE_QUOTE">DOUBLE_QUOTE (")</option>
                                    <option value="SINGLE_QUOTE">SINGLE_QUOTE (')</option>
                                </select>
                            </div>
                        </div>

                        <div className="form-row col-6">
                            <label htmlFor="colFormLabelSm" className="col-3 col-form-label">Separador</label>

                            <div className="col-9">
                                <select className="custom-select" name="separador" value={this.state.separador} onChange={this.handleChange}>
                                    <option value="COMMA">COMMA</option>
                                    <option value="SEMICOLON">SEMICOLON</option>
                                    <option value="TAB">TAB</option>
                                    <option value="PIPE">PIPE</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <hr className="my-4" />

                    <div className="form-row fRow">
                        <div className="form-row col-6">
                            <label htmlFor="colFormLabelSm" className="col-3 col-form-label">Categoria</label>

                            <div className="col-9">
                                <select className="custom-select" name="reportType" value={this.state.reportType} onChange={this.handleChange}>
                                    {keys.map((k, i) => {
                                        return <option key={"Cat_" + (i + 1)} value={k}>{k}</option>
                                    })}
                                </select>
                            </div>
                        </div>

                        <div className="form-row col-6">
                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="filter" onChange={this.handleChange} hidden/>
                        </div>
                    </div>

                    {this.getCategories()}

                    <hr className="my-4" />

                    <div className="form-row fRow justify-content-center">
                        <div className="col-6 text-center">
                            <button type="button" className="btn btn-outline-primary" onClick={this.preview}>Preview</button>
                        </div>

                        <div className="col-6 text-center">
                            <button type="button" className="btn btn-outline-success" onClick={this.schedule}>Schedule</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}