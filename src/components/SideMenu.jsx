import React, { Component } from 'react';
import { MDBIcon } from 'mdbreact';

import '../css/SideMenu.css';

export class SideMenu extends Component {
    render() {
        return (
            <div className="sideMenu">
                <div className="userInfo">
                    <a className="userInfoT1">Merchant ID: 1</a>

                    <div className="userInfoP1">
                        <div className="img-circle icon-co"></div>

                        <div className="userSideInfo">
                            <a class="nav-link dropdown-toggle">
                                Pagosonline.com
                            </a>
                            <small>Cuenta 1</small>
                        </div>
                    </div>

                    <div className="userInfoT2">
                        <a class="nav-link pb-0">
                            $130,846,774.80 COP
                            </a>
                        <small>Saldo disponible</small>
                    </div>
                </div>

                <ul class="navbar-nav sideUl">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span style={{ marginRight: "3px" }}><MDBIcon icon="home" /></span>
                            Inicio
                        </a>
                    </li>

                    <li class="nav-item dropdown menuDrop active">
                        <a class="nav-link dropdown-toggle" id="sideMDrop1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span style={{ marginRight: "15px" }}><MDBIcon icon="chart-area" /></span>
                            Transacciones
                        </a>

                        <div class="dropdown-menu" aria-labelledby="sideMDrop1">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>

                    <li class="nav-item menuItem semiActive">
                        <a class="nav-link" href="#">Reporte de ventas</a>
                    </li>

                    <li class="nav-item menuItem semiActive">
                        <a class="nav-link" href="#">Disputas</a>
                    </li>

                    <li class="nav-item menuItem semiActive">
                        <a class="nav-link" href="#">Balance financiero</a>
                    </li>

                    <li class="nav-item menuItem semiActive">
                        <a class="nav-link" href="#">Certificado de Retenciones</a>
                    </li>

                    <li class="nav-item menuItem active">
                        <a class="nav-link" href="#">Programar reporte</a>
                    </li>

                    <li class="nav-item menuItem semiActive">
                        <a class="nav-link" href="#">Actualizar reportes</a>
                    </li>

                    <li class="nav-item menuItem semiActive">
                        <a class="nav-link" href="#">Eliminar reportes</a>
                    </li>

                    <li class="nav-item menuItem semiActive">
                        <a class="nav-link" href="#">Logs de reportes</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="sideMDrop2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span style={{ marginRight: "15px" }}><MDBIcon icon="exchange-alt" /></span>
                            Transferencias
                        </a>

                        <div class="dropdown-menu" aria-labelledby="sideMDrop2">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="sideMDrop3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span style={{ marginRight: "15px" }}><MDBIcon icon="file-alt" /></span>
                            Recaudo
                        </a>

                        <div class="dropdown-menu" aria-labelledby="sideMDrop3">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="sideMDrop4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span style={{ marginRight: "15px" }}><MDBIcon icon="shopping-cart" /></span>
                            Cobra con PayU
                        </a>

                        <div class="dropdown-menu" aria-labelledby="sideMDrop4">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}
