import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon, MDBInput } from 'mdbreact'
import '../css/login.css'
import React from 'react'
import logo from '../resource/payULogo.png'
import * as axios from "axios"

export class Login extends React.Component {

    constructor(props) {
        super(props);

        this.submit = this.submit.bind(this);
    }

    submit() {
        const main = this;
        const email = document.getElementById('email').value
        const password = document.getElementById('password').value

        axios.post('https://api.payulatam.com/secure-api/authorization/login', {
            login: email,
            password: password
        })
        .then(function (response) {
            console.log("Logged in...");
            localStorage.setItem("accessToken","Bearer " + response.headers.jwt_auth)
            localStorage.setItem("isLoggedIn", true)
            main.props.logIn();
        })
        .catch(function (error) {
            alert("Failed when try to login");
            console.log(error);
        });
    }

    render() {        
        return (
            <div className="bg">
                <MDBContainer>
                    <MDBRow>
                        <MDBCol sm="4"/>
                        <MDBCol sm="4" style={{marginTop: '5%',
                            backgroundColor: 'rgba(255, 255, 255, 0.76)',
                            'borderRadius': '46px'}}>
                            <MDBContainer>
                                <MDBRow>
                                    <MDBCol sm="1"/>
                                    <MDBCol sm="7">
                                        <img src={logo} alt="Logo" />
                                    </MDBCol>
                                    <MDBCol sm="4"/>
                                </MDBRow>
                            </MDBContainer>
                            <form>
                                <br/>
                                <p className="h5 text-center mb-4">Bienvenido a tu módulo PayU</p>
                                <div className="grey-text">
                                    <MDBInput
                                        id='email'
                                        label="usuario@mail.com"
                                        icon="user"
                                        group
                                        type="email"
                                        validate
                                        error="wrong"
                                        success="right"
                                    />
                                    <MDBInput
                                        id='password'
                                        label="contraseña"
                                        icon="lock"
                                        group
                                        type="password"
                                        validate
                                        error="wrong"
                                        success="right"
                                    />
                                </div>
                                <div className="text-center">
                                    <MDBBtn style={{background:'rgb(154, 180, 50)', 'borderRadius': '46px', color: 'white'}}
                                            color='rgb(142, 46, 56)' onClick={this.submit}>
                                        Ingresar
                                        <MDBIcon far icon="paper-plane" className="ml-1" />
                                    </MDBBtn>
                                    <br/><br/>
                                    <br/>
                                </div>
                            </form>
                        </MDBCol>
                        <MDBCol sm="4"/>
                    </MDBRow>
                </MDBContainer>
            </div>
        )
    }
}